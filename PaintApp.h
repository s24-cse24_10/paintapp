#ifndef PAINT_APP_H
#define PAINT_APP_H

#include <iostream>
#include "Rectangle.h"
#include "TextureButton.h"
#include "Button.h"
#include "Color.h"
#include "Point.h"

enum TOOL {PAINT, ERASER};
enum COLOR {RED, GREEN, BLUE};

const int maxPoints = 1000;

struct PaintApp {
    Rectangle verticalToolbar;
    Rectangle horizontalToolbar;
    Rectangle canvas;
    Rectangle redButton;
    Rectangle greenButton;
    Rectangle blueButton;

    TextureButton pencilButton;
    TextureButton eraserButton;
    Button clearButton;

    TOOL selectedTool;
    COLOR selectedColor;

    Color brushColor;
    Point points[maxPoints];
    int pointsCount;
    
    PaintApp() {
        redButton = Rectangle(-0.8f, -0.8f, 0.2f, 0.2f, Color(1.0f, 0.0f, 0.0f));
        greenButton = Rectangle(-0.6f, -0.8f, 0.2f, 0.2f, Color(0.0f, 1.0f, 0.0f));
        blueButton = Rectangle(-0.4f, -0.8f, 0.2f, 0.2f, Color(0.0f, 0.0f, 1.0f));

        verticalToolbar = Rectangle(-1.0f, 1.0f, 0.2f, 2.0f, Color(0.7f, 0.7f, 0.7f));
        horizontalToolbar = Rectangle(-0.8f, -0.8f, 1.8f, 0.2f, Color(0.7f, 0.7f, 0.7f));
        canvas = Rectangle(-0.8f, 1.0f, 1.8f, 1.8f, Color(0.9f, 0.9f, 0.9f));

        pencilButton = TextureButton("assets/pencil.png", -1.0f, 1.0f, 0.2f, 0.2f);
        eraserButton = TextureButton("assets/eraser.png", -1.0f, 0.8f, 0.2f, 0.2f);
        clearButton = Button("Clear", 0.7f, -0.8f);

        brushColor.setRed();

        selectedTool = PAINT;
        selectedColor = RED;
        pointsCount = 0;
    }

    void init() {
        pencilButton.loadTexture();
        eraserButton.loadTexture();
    }

    void handleLeftMouseDown(float x, float y) {
        if (pencilButton.isClicked(x, y)) {
            selectedTool = PAINT;
        } else if (eraserButton.isClicked(x, y)) {
            selectedTool = ERASER;
        }

        if (clearButton.isClicked(x, y)) {
            pointsCount = 0;
        }

        if (redButton.isClicked(x, y)) {
            selectedColor = RED;
        } else if (greenButton.isClicked(x, y)) {
            selectedColor = GREEN;
        } else if (blueButton.isClicked(x, y)) {
            selectedColor = BLUE;
        }

        if (canvas.isClicked(x, y)) {
            if (pointsCount < maxPoints) {
                points[pointsCount] = Point(x, y, brushColor);
                pointsCount++;
            }
        }
    }

    void handleLeftMouseUp(float x, float y){
        // 
    }

    void handleRightMouseDown(float x, float y){
        // 
    }

    void handleRightMouseUp(float x, float y){
        // 
    }

    void handleKeyboardDown(unsigned char key, int x, int y) {
        //
    }

    void handleMouseMotion(float x, float y) {
        if (canvas.isClicked(x, y)) {
            if (pointsCount < maxPoints) {
                points[pointsCount] = Point(x, y, brushColor);
                pointsCount++;
            }
        }
    }

    void render() {
        if (selectedColor == RED) {
            brushColor.setRed();
            redButton.selected = true;
            greenButton.selected = false;
            blueButton.selected = false;
        } else if (selectedColor == GREEN) {
            brushColor.setGreen();
            redButton.selected = false;
            greenButton.selected = true;
            blueButton.selected = false;
        } else if (selectedColor == BLUE) {
            brushColor.setBlue();
            redButton.selected = false;
            greenButton.selected = false;
            blueButton.selected = true;
        }

        if (selectedTool == PAINT) {
            pencilButton.selected = true;
            eraserButton.selected = false;
        } else if (selectedTool == ERASER) {
            brushColor.setColor(0.9f, 0.9f, 0.9f);
            pencilButton.selected = false;
            eraserButton.selected = true;
            redButton.selected = false;
            greenButton.selected = false;
            blueButton.selected = false;
        }

        canvas.draw();

        for (int i = 0; i < pointsCount; i++) {
            points[i].draw();
        }

        verticalToolbar.draw();
        horizontalToolbar.draw();
        
        redButton.draw();
        greenButton.draw();
        blueButton.draw();

        pencilButton.draw();
        eraserButton.draw();
        clearButton.draw();
    }

};

#endif